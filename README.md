# android_vendor_bloatware

there's a mix of bloatware apps

### List of Apps

- DuckDuckGo
- GCam Mod
- Mi Calculator
- Mi Remote
- MiXplorer

### How to use

1. clone into your rom folder

    `vendor/bloatware`

   or write in your local_manifests

    `<project path="vendor/bloatware" name="hariimurti/android_vendor_bloatware" remote="gitlab" revision="master" clone-depth="1" />`

2. write this code in your device tree

    `$(call inherit-product, vendor/bloatware/packages.mk)`

3. build your rom
