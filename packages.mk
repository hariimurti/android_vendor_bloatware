# set true if gcam mod override other camera
# TARGET_GCAMMOD_AS_DEFAULT_CAM := false

# system priv-app
PRODUCT_PACKAGES += \
    GCamMod

# product app
PRODUCT_PACKAGES += \
    MiCalculator \
    MiRemote \
    MiXplorer

ifneq ($(TARGET_GAPPS_LITE),false)
PRODUCT_PACKAGES += \
    DuckDuckGo
endif
